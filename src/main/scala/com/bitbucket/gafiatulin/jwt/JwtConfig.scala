package com.bitbucket.gafiatulin.jwt

import com.typesafe.config.{Config, ConfigValueType}

import scala.collection.JavaConverters._
import scala.util.Try

case class JwtConfig(issuer: String, secrets: Map[String, String])

case object JwtConfig{
  def fromConfig(c: Config): JwtConfig = JwtConfig(
    c.getString("issuer"),
    c.getConfig("secrets").entrySet().asScala.flatMap{
      case e if e.getValue.valueType == ConfigValueType.STRING =>
        Try(e.getValue.unwrapped().asInstanceOf[String]).map(e.getKey -> _).toOption
      case _ => None
    }.toMap
  )
}
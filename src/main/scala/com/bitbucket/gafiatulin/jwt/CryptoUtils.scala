package com.bitbucket.gafiatulin.jwt

import java.util.Base64
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

import com.nimbusds.jose.crypto.{DirectDecrypter, MACSigner, MACVerifier}

private [jwt] case class CryptoUtils(secretKey: SecretKey, signer: MACSigner, directDecrypter: DirectDecrypter, macVerifier: MACVerifier)

private [jwt] object CryptoUtils{
  private final def generateSecretKey(secret: String): SecretKey = {
    val decodedKey = Base64.getDecoder.decode(secret)
    new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES")
  }
  def fromSeed(seedString: String): CryptoUtils = {
    val key = generateSecretKey(seedString)
    val signer = new MACSigner(key.getEncoded)
    val decrypter = new DirectDecrypter(key.getEncoded)
    val verifier = new MACVerifier(key.getEncoded)
    CryptoUtils(key, signer, decrypter, verifier)
  }
}
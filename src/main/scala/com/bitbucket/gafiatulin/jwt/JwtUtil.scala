package com.bitbucket.gafiatulin.jwt

import java.util.concurrent.ConcurrentHashMap

import com.nimbusds.jose._
import com.nimbusds.jose.crypto.DirectEncrypter
import com.nimbusds.jwt.{JWTClaimsSet, SignedJWT}

import scala.collection.JavaConverters._
import scala.util.Try
import scala.util.control.NonFatal

case class JwtUtil(jwtConfig: JwtConfig) {

  private val utilsMap: ConcurrentHashMap[String, CryptoUtils] = new ConcurrentHashMap()
  private def getCrypto(keyName: String): CryptoUtils = Option(utilsMap.get(keyName)).getOrElse{
    val v = CryptoUtils.fromSeed(jwtConfig.secrets(keyName))
    utilsMap.putIfAbsent(keyName, v)
    v
  }
  private def wrapKeyCheck[T](keyName: String)(f: => T): T =
    if(jwtConfig.secrets.contains(keyName)) f else throw JWTException("Unknown keyName: " + keyName)

  def decryptAndVerify(jweString: String, keyName: String): JWTClaimsSet = wrapKeyCheck(keyName){
    val cu = getCrypto(keyName)
    val jwe = JWEObject.parse(jweString)
    jwe.decrypt(cu.directDecrypter)
    val sJWT = jwe.getPayload.toSignedJWT
    if (sJWT.verify(cu.macVerifier)) sJWT.getJWTClaimsSet else throw JWTException("Wrong JWT signature")
  }

  def serializedEncryptedJWE(claimsSet: JWTClaimsSet, keyName: String): String = wrapKeyCheck(keyName){
    val cu = getCrypto(keyName)
    val signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet)
    signedJWT.sign(cu.signer)
    val jweObject = new JWEObject(
      new JWEHeader.Builder(JWEAlgorithm.DIR, EncryptionMethod.A256GCM).contentType("JWT").build(),
      new Payload(signedJWT)
    )
    jweObject.encrypt(new DirectEncrypter(cu.secretKey.getEncoded))
    jweObject.serialize()
  }

  def fromToken[T](jweString: String, keyName: String)(implicit reader: JWTClaimSetReader[T]): T =
    reader.fromJWT(decryptAndVerify(jweString, keyName))

  def toToken[T](obj: T, keyName: String)(implicit writer: JWTClaimSetWriter[T]): String =
    serializedEncryptedJWE(writer.toJWT(obj), keyName)

  def fromTokenEither[T](jweString: String, keyName: String)(implicit reader: JWTClaimSetReader[T]): Either[JWTException, T] =
    try {
      Right(fromToken(jweString, keyName))
    } catch {
      case e: JWTException => Left(e)
      case NonFatal(e) => Left(JWTException(Option(e.getMessage).getOrElse("Unknown Exception")))
    }

  def toTokenEither[T](obj: T, keyName: String)(implicit writer: JWTClaimSetWriter[T]): Either[JWTException, String] =
    try{
      Right(toToken(obj, keyName))
    } catch {
      case e: JWTException => Left(e)
      case NonFatal(e) => Left(JWTException(Option(e.getMessage).getOrElse("Unknown Exception")))
    }

  def claimsSetFromStringMap(map: Map[String, String]): JWTClaimsSet =
    map.foldRight(new JWTClaimsSet.Builder()){case ((k, value), builder) => builder.claim(k, value)}.issuer(jwtConfig.issuer).build()

  def claimsSetToStringMap(cs: JWTClaimsSet, removeIssuer: Boolean = false): Map[String, String] = {
    cs.getClaims.asScala.foldRight(Map.empty[String, String]){ case ((k, v), acc) =>
      if (removeIssuer & k == "iss") acc else Try(v.toString).map(acc.updated(k, _)).getOrElse(acc)
    }
  }
}
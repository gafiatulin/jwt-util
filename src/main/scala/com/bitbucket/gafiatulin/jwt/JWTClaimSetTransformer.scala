package com.bitbucket.gafiatulin.jwt

import com.nimbusds.jwt.JWTClaimsSet

/**
  * Created by victor on 20/02/2017.
  */

trait JWTClaimSetReader[T]{
  def fromJWT(cs: JWTClaimsSet): T
}

object JWTClaimSetReader{
  implicit def func2Reader[T](f: JWTClaimsSet => T): JWTClaimSetReader[T] =
    (cs: JWTClaimsSet) => f(cs)
  implicit def strFunc2Reader[T1, T](f: String => T1, g: T1 => T): JWTClaimSetReader[T] =
    (cs: JWTClaimsSet) => g(f(cs.toJSONObject.toJSONString))
}

trait JWTClaimSetWriter[T]{
  def toJWT(obj: T): JWTClaimsSet
}

object JWTClaimSetWriter{
  implicit def func2Writer[T](f: T => JWTClaimsSet): JWTClaimSetWriter[T] =
    (obj: T) => f(obj)
  implicit def strFunc2Writer[T1, T](f: T => T1, g: T1 => String): JWTClaimSetWriter[T] =
    (obj: T) => JWTClaimsSet.parse(g(f(obj)))
}

trait JWTClaimSetTransformer[T] extends JWTClaimSetReader[T] with JWTClaimSetWriter[T]

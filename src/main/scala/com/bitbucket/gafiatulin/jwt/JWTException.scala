package com.bitbucket.gafiatulin.jwt

/**
  * Created by victor on 20/02/2017.
  */

final case class JWTException(message: String) extends Exception(message)
